import time

class UiHelper(object):

    xpath_search_bar = "//input[@name='q']"
    xpath_search_btn = "//input[@name='btnK']"
    xpath_dash_site = "//a[contains(@href,'www.dash.org')]" #search result - www.dash.org site
    xpath_search_btn_expanded = "//input[@type='button' and contains(@value,'Google')]" #search button for when suggestion searches appear

    def __init__(self, driver):
        self.driver = driver

    def SlowSearch(self, what_to_search, time_between_char_strokes_milis):
        self.driver.find_element_by_xpath(self.xpath_search_bar).clear()
        for char in what_to_search:
            self.driver.find_element_by_xpath(self.xpath_search_bar).send_keys(char)
            time.sleep(time_between_char_strokes_milis/1000)
        self.driver.find_element_by_xpath(self.xpath_search_btn_expanded).click()

    def Search(self, what_to_search):
        self.driver.find_element_by_xpath(self.xpath_search_bar).clear()
        self.driver.find_element_by_xpath(self.xpath_search_bar).send_keys(what_to_search)
        self.driver.find_element_by_xpath(self.xpath_search_btn).click()

    def CheckResults(self):
        return "www.dash.org" in self.driver.find_element_by_xpath(self.xpath_dash_site).get_attribute('href')