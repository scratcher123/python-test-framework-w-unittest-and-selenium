from selenium import webdriver
from APIHelper import APIHelper
import unittest


class API_Suite(unittest.TestCase):

    @classmethod
    def setUp(self):
        # do something if needed (initialize logger etc.)
        pass

    # Slow search so that search suggestions show up
    def test_1_api_get_all_users(self):
        api = APIHelper()
        response = api.getAllUsers()
        print(response.text)
        assert "a123121ws1s1s1s21s1212s12s12s12s12s" in response.text


    @classmethod
    def tearDown(self):
        # do something if needed
        pass


if __name__ == '__main__':
    unittest.main()
