from selenium import webdriver
from UiHelper import UiHelper
import unittest

class Let_Me_Google_That(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)
        self.base_url = "https://google.com"
        self.driver.get(self.base_url + "/")

    # Slow search so that search suggestions show up
    def test_1_slow_search(self):
        web_browser = UiHelper(self.driver)
        web_browser.SlowSearch("DASH.org", 500)
        assert web_browser.CheckResults()

    # Normal search with no search suggestions
    def test_2_normal_search(self):
        web_browser = UiHelper(self.driver)
        web_browser.Search("DASH.org")
        assert web_browser.CheckResults()

    @classmethod
    def tearDown(self):
        self.driver.close()

if __name__ == '__main__':
    unittest.main()
