import time
import requests

class APIHelper(object):

    base_URL = "https://gorest.co.in/"

    def __init__(self):
        pass

    def getAllUsers(self):
        response = requests.get(self.base_URL+"/public-api/users")
        return response
